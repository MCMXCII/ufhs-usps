<?php
class Ufhs_Usp_Block_Adminhtml_Attributes_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
	public function __construct()
	{
		parent::__construct();
		$this->setId('uspAttributesGrid');
		$this->setDefaultSort('id');
		$this->setDefaultDir('ASC');
		$this->setSaveParametersInSession(true);
	}

	protected function _prepareCollection()
	{
		$collection = Mage::getModel('usp/attribute')->getCollection();
		$collection->setOrder('group_id', 'ASC');
		$collection->setOrder('id', 'ASC');
		$this->setCollection($collection);
		return parent::_prepareCollection();
	}

	protected function _prepareColumns()
	{
		$this->addColumn('id', array(
			'header' => Mage::helper('usp')->__('ID'),
			'align' => 'left',
			'width' => '10px',
			'index' => 'id'
		));
		$this->addColumn('name', array(
			'header' => Mage::helper('usp')->__('Name'),
			'align' => 'left',
			'width' => '50px',
			'index' => 'name'
		));
		$this->addColumn('group', array(
			'header' => Mage::helper('usp')->__('Group'),
			'align' => 'left',
			'width' => '50px',
			'index' => 'group_id',
			'renderer' => 'Ufhs_Usp_Block_Adminhtml_Renderers_Groupname'
		));

		$object = new Varien_Object(array('grid_block' => $this));
		Mage::dispatchEvent("usp_block_adminhtml_attributes_grid_preparecolumns", array("data" => $object));
		return parent::_prepareColumns();
	}

	public function getRowUrl($row)
	{
		return $this->getUrl('*/*/editattrib', array('id' => $row->getData()['id']));
	}

	public function getEmptyText()
	{
		return $this->__('No attributes have been created yet.');
	}
}