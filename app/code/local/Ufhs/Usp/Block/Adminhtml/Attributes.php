<?php
class Ufhs_Usp_Block_Adminhtml_Attributes extends Mage_Adminhtml_Block_Widget_Grid_Container
{
	public function __construct()
	{
		$this->_controller = 'adminhtml_attributes';
		$this->_blockGroup = 'usp';
		$this->_headerText = Mage::helper('usp')->__('Attributes');
		parent::__construct();
		$this->_updateButton('add', 'onclick',"setLocation('" . $this->getUrl('*/*/newattrib') . "')");
	}
}