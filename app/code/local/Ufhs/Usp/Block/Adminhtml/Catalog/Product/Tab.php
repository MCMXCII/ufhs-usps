<?php
class Ufhs_Usp_Block_Adminhtml_Catalog_Product_Tab extends Mage_Adminhtml_Block_Template
implements Mage_Adminhtml_Block_Widget_Tab_Interface
{

    /**
     * Set the template for the block
     *
     */
    public function _construct()
    {
        parent::_construct();
        $this->setTemplate('usp/catalog/product/tab.phtml');
    }

    /**
     * Retrieve the label used for the tab relating to this block
     *
     * @return string
     */
    public function getTabLabel()
    {
        return $this->__('USP');
    }

     /**
     * Retrieve the title used by this tab
     *
     * @return string
     */
     public function getTabTitle()
     {
        return $this->__('USP');
    }

    /**
     * Determines whether to display the tab
     * Add logic here to decide whether you want the tab to display
     *
     * @return bool
     */
    public function canShowTab()
    {
        return true;
    }

    /**
     * Stops the tab being hidden
     *
     * @return bool
     */
    public function isHidden()
    {
        return false;
    }

    public function getSelectedList() {
    	$data = Mage::getModel('usp/map')->getCollection()->addFieldToFilter('prod_id',$this->getRequest()->getParam('id'))->addFieldToSelect('attrib_id')->getData();
    	return array_map(function ($item) {
    		return $item['attrib_id'];
    	}, $data);
    }

    public function getAttributes() {
    	$return = [];
    	$groups = [];
    	$tmpGroups = Mage::getModel('usp/attributegroup')->getCollection()->getData();
    	foreach ($tmpGroups as $group) {
    		$groups[$group['id']] = $group['name'];
    	}
       	$attributes = Mage::getModel('usp/attribute')->getCollection()->getData();
       	foreach ($attributes as $attribute) {
       		$gname = $groups[$attribute['group_id']];
       		if(!isset($return[$gname])) {
       			$return[$gname] = [];
       		}
       		$return[$gname][$attribute['id']] = $attribute['name'];
       	}
       	return $return;
    }
}