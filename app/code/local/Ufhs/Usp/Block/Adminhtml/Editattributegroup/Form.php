<?php
class Ufhs_Usp_Block_Adminhtml_Editattributegroup_Form extends Mage_Adminhtml_Block_Widget_Form
{
	protected function _construct()
	{
		parent::_construct();
		$this->setFormTitle('Edit Attribute Group');
	}

	protected function _prepareForm()
	{
		$id = $this->getRequest()->getParam('id');
		$group = Mage::getModel('usp/attributegroup')->load($id);

		$form = new Varien_Data_Form(array(
			'id' => 'editattributegroup',
			'action' => $this->getUrl('*/*/addattribgroup', array('id' => $this->getRequest()->getParam('id'))),
			'method' => 'post',
			'enctype' => 'multipart/form-data'
		));
		$form->setUseContainer(true);
		$this->setForm($form);

		$fieldset = $form->addFieldset('editattrib_form', array(
			'legend' => Mage::helper('usp')->__('Attribute Details')
		));

		$fieldset->addField('name', 'text', array(
			'label' => Mage::helper('usp')->__('Name'),
			'class' => 'required-entry',
			'required' => true,
			'name' => 'name',
			'value' => $group->getName()
		));

		return parent::_prepareForm();
	}
}