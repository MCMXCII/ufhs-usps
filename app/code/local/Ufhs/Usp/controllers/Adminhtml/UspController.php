<?php

class Ufhs_Usp_Adminhtml_UspController extends Mage_Adminhtml_Controller_action
{
	protected function _initAction()
	{
		$this->_title($this->__('USP'));
		$this->loadLayout();
		$this->_initLayoutMessages('adminhtml/session');
		$this->_setActiveMenu('ufhs');
		return $this;
	}

	private function _outputLayout($title)
	{
		$this->_initAction();
		$this->_title($this->__($title));
		$this->renderLayout();
	}

	/**
	 * Layout Functions
	 */
	public function viewattribAction() {
		$this->_outputLayout('Attributes');
	}
	public function viewattribgroupAction() {
		$this->_outputLayout('Attribute Groups');
	}
	public function newattribAction() {
		$this->_outputLayout('New Attribute');
	}
	public function editattribAction() {
		$this->_outputLayout('Edit Attribute');
	}
	public function newgroupAction() {
		$this->_outputLayout('New Attribute Group');
	}
	public function editgroupAction() {
		$this->_outputLayout('Edit Attribute Group');
	}

	/**
	 * Object Manipulation Functions
	 */
	public function addattribAction() {
		$post = Mage::app()->getRequest()->getParams();
		if (!empty($post['name']) && !empty($post['group_id'])) {
			$group = Mage::getModel('usp/attributegroup')->load($post['group_id']);
			if (!empty($group->getId()) && strlen($post['name']) > 1) {

				$attribute = Mage::getModel('usp/attribute');

				if (!empty($post['id'])) {
					$attribute = $attribute->load($post['id']);
				}

				$attribute->setName($post['name'])
				->setGroupId($post['group_id']);

				if ($_FILES['image']['error'] == 0) {
					$uploader = new Varien_File_Uploader('image');
					if($uploader->setAllowRenameFiles(true)->save(Mage::getBaseDir('media') . '/ufhs/usp/')) {
						$attribute->setImage($uploader->getUploadedFileName());
					} else {
						Mage::getSingleton('adminhtml/session')->addError(Mage::helper('usp')->__('Error saving that icon.'));
					}
				}

				$attribute->save();

			} else {
				Mage::getSingleton('adminhtml/session')->addError(Mage::helper('usp')->__('Please provide valid data.'));
			}
		} else {
			Mage::getSingleton('adminhtml/session')->addError(Mage::helper('usp')->__('You seem to be missing some data.'));
		}
		$this->_redirect('*/*/viewattrib');
	}

	public function addattribgroupAction() {
		$post = Mage::app()->getRequest()->getParams();
		if (!empty($post['name']) && strlen($post['name']) > 1) {
			$group = Mage::getModel('usp/attributegroup');
			if (!empty($post['id'])) {
				$group = $group->load($post['id']);
			}
			$group->setName($post['name'])
			->save();
		} else {
			Mage::getSingleton('adminhtml/session')->addError(Mage::helper('usp')->__('Please provide a valid name.'));
		}
		$this->_redirect('*/*/viewattribgroup');
	}
}